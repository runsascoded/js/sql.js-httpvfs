# @rdub/react-sql.js-httpvfs
React wrapper for [sql.js-httpvfs] (for querying SQLite databases from a web browser).

<a href="https://npmjs.org/package/@rdub/react-sql.js-httpvfs" title="View @rdub/react-sql.js-httpvfs on NPM"><img src="https://img.shields.io/npm/v/@rdub/react-sql.js-httpvfs.svg" alt="@rdub/react-sql.js-httpvfs NPM version" /></a>

## API
- [query.ts](src/query.ts): React hooks for querying SQLite databases
  - `useSqlQuery`
  - `useSqlQueryLazy`
  - `useSqlResult`
  - `useSqlQueryCallback`
- [sql.ts]: lower-level bindings to [sql.js-httpvfs]
  - `getDb`
  - `useDb`

## Examples

### [crashes.hudcostreets.org]

Paginated table backed by SQLite file in S3:

[![](screenshots/table.gif)][demo]

[demo], [source], [data](https://nj-crashes.s3.amazonaws.com/index.html#/njsp/data) ([`s3://nj-crashes/njsp/crashes.db`])

[sql.js-httpvfs]: https://github.com/phiresky/sql.js-httpvfs

[source]: https://github.com/hudcostreets/nj-crashes/blob/512e7cc79c1fb799f7e97b764d77a3f0645f6533/www/src/use-njsp-crashes.tsx#L83
[demo]: https://crashes.hudcostreets.org/#recent-fatal-crashes
[crashes.hudcostreets.org]: https://crashes.hudcostreets.org
[query.ts]: src/query.ts
[sql.ts]: src/sql.ts

[`s3://nj-crashes/njsp/crashes.db`]: https://nj-crashes.s3.amazonaws.com/njsp/data/crashes.db
