import { Either } from "fp-ts/Either";
export type Result<T> = Either<Error, T[]>;
export type Base = {
    url: string;
    requestChunkSize?: number;
    maxBytesToRead?: number;
    timerId?: string;
};
export type Query = Base & {
    query: string;
};
export declare function useSqlQueryCallback<T = any>({ url, requestChunkSize, maxBytesToRead, timerId, }: Base): (query: string) => Promise<Result<T>> | null;
export declare function useSqlResult<T = any>({ setResult, ...base }: {
    setResult: (result: Result<T>) => (Promise<void> | void | null);
} & Base): (query: string) => (Promise<void> | null);
export declare function useSqlQuery<T = any>({ query, init, ...base }: Query & {
    init: T[];
}): Result<T>;
export declare function useSqlQueryLazy<T = any>({ query, ...base }: Query): Result<T> | null;
