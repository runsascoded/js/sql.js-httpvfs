import { useDb } from "./sql";
import { useCallback, useEffect, useMemo, useState } from "react";
import { left, right } from "fp-ts/Either";
import { Timer } from "@rdub/base/time";
export function useSqlQueryCallback({ url, requestChunkSize, maxBytesToRead, timerId, }) {
    const dbProps = useMemo(() => ({ url, requestChunkSize }), [url, requestChunkSize]);
    const dbOpts = useMemo(() => ({ timerId, maxBytesToRead }), [timerId, maxBytesToRead,]);
    const { db } = useDb(dbProps, dbOpts);
    const callback = useCallback((query) => {
        if (!db || !query)
            return null;
        const queryTimer = new Timer(timerId, `ran query ${query}`);
        queryTimer.start();
        return db.query(query)
            .then(rows => {
            queryTimer.end();
            console.log(`rows:`, rows);
            return right(rows);
        })
            .catch(err => left(err));
    }, [db]);
    return callback;
}
export function useSqlResult({ setResult, ...base }) {
    const queryCallback = useSqlQueryCallback(base);
    const doQuery = useCallback((query) => {
        const result = queryCallback(query);
        if (!result)
            return null;
        return result.then(res => {
            void setResult(res);
        });
    }, [queryCallback]);
    return doQuery;
}
export function useSqlQuery({ query, init, ...base }) {
    const callback = useSqlQueryCallback(base);
    const [result, setResult] = useState(right(init));
    useEffect(() => {
        const result = callback(query);
        result?.then(res => setResult(res));
    }, [callback, query]);
    return result;
}
export function useSqlQueryLazy({ query, ...base }) {
    const callback = useSqlQueryCallback(base);
    const [result, setResult] = useState(null);
    useEffect(() => {
        const result = callback(query);
        result?.then(res => setResult(res));
    }, [callback, query]);
    return result;
}
