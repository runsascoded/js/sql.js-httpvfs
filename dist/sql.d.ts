import { WorkerHttpvfs } from "sql.js-httpvfs";
import { Remote } from "comlink";
import { LazyHttpDatabase, SplitFileConfig, SplitFileConfigInner } from "sql.js-httpvfs/dist/sqlite.worker";
import { Timer } from "@rdub/base/time";
export type Db = Remote<LazyHttpDatabase>;
export type Props = SplitFileConfigInner | SplitFileConfig[] | {
    url: string;
    requestChunkSize?: number;
};
export type Opts = {
    maxBytesToRead?: number;
    timerId?: string;
    openTimer?: Timer;
};
export declare const DefaultRequestChunkSize = 4096;
export declare function getWorker(props: Props, opts?: Opts): Promise<WorkerHttpvfs>;
export declare function getDb(props: Props, opts?: Opts): Promise<Db>;
export type UseDb = {
    db: Db | null;
    dbId: number;
};
export declare function useDb(props: Props, opts?: Opts): UseDb;
