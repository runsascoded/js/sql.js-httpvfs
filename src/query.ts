import * as sql from "./sql";
import { useDb } from "./sql";
import { useCallback, useEffect, useMemo, useState } from "react";
import { Either, left, right } from "fp-ts/Either";
import { Timer } from "@rdub/base/time"

export type Result<T> = Either<Error, T[]>

export type Base = {
    url: string
    requestChunkSize?: number
    maxBytesToRead?: number
    timerId?: string
}

export type Query = Base & {
    query: string
}

export function useSqlQueryCallback<T = any>(
    {
        url,
        requestChunkSize,
        maxBytesToRead,
        timerId,
    }: Base
): (query: string) => Promise<Result<T>> | null {
    const dbProps: sql.Props = useMemo(() => ({ url, requestChunkSize }), [ url, requestChunkSize ])
    const dbOpts = useMemo(() => ({ timerId, maxBytesToRead }), [ timerId, maxBytesToRead, ])
    const { db } = useDb(dbProps, dbOpts)
    const callback = useCallback(
        (query: string): Promise<Result<T>> | null => {
            if (!db || !query) return null
            const queryTimer = new Timer(timerId, `ran query ${query}`)
            queryTimer.start()
            return db.query(query)
                .then(
                    rows => {
                        queryTimer.end()
                        console.log(`rows:`, rows)
                        return right(rows as T[])
                    }
                )
                .catch(err => left(err as Error))
        },
        [ db ]
    )
    return callback
}

export function useSqlResult<T = any>(
    {
        setResult,
        ...base
    }: {
        setResult: (result: Result<T>) => (Promise<void> | void | null)
    } & Base
): (query: string) => (Promise<void> | null) {
    const queryCallback = useSqlQueryCallback(base)
    const doQuery = useCallback(
        (query: string) => {
            const result = queryCallback(query)
            if (!result) return null
            return result.then(res => {
                void setResult(res)
            })
        },
        [queryCallback]
    )
    return doQuery
}

export function useSqlQuery<T = any>({ query, init, ...base }: Query & { init: T[] }): Result<T> {
    const callback = useSqlQueryCallback(base)
    const [ result, setResult ] = useState<Result<T>>(right(init))
    useEffect(
        () => {
            const result = callback(query)
            result?.then(res => setResult(res))
        },
        [ callback, query ]
    )
    return result
}

export function useSqlQueryLazy<T = any>({ query, ...base }: Query): Result<T> | null {
    const callback = useSqlQueryCallback(base)
    const [ result, setResult ] = useState<Result<T> | null>(null)
    useEffect(
        () => {
            const result = callback(query)
            result?.then(res => setResult(res))
        },
        [ callback, query ]
    )
    return result
}
