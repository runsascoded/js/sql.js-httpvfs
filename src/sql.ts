import { createDbWorker, WorkerHttpvfs } from "sql.js-httpvfs"
import { Remote } from "comlink"
import { LazyHttpDatabase, SplitFileConfig, SplitFileConfigInner } from "sql.js-httpvfs/dist/sqlite.worker"
import { useMemo, useRef, useState } from "react";
import { useDeepCompareEffect } from "use-deep-compare";
import { Timer } from "@rdub/base/time"

export type Db = Remote<LazyHttpDatabase>

export type Props = SplitFileConfigInner | SplitFileConfig[] | {
    url: string
    requestChunkSize?: number
}
export type Opts = {
    maxBytesToRead?: number
    timerId?: string
    openTimer?: Timer
}

export const DefaultRequestChunkSize = 4096

export async function getWorker(props: Props, opts: Opts = {}): Promise<WorkerHttpvfs> {
    const workerUrl = new URL("sql.js-httpvfs/dist/sqlite.worker.js", import.meta.url);
    const wasmUrl = new URL("sql.js-httpvfs/dist/sql-wasm.wasm", import.meta.url);
    const configs: SplitFileConfig[] = Array.isArray(props) ? props : [{
        from: "inline",
        config: {
            serverMode: "full",
            ...props,
            requestChunkSize: props.requestChunkSize ?? DefaultRequestChunkSize,
        }
    }]
    const { timerId } = opts
    const workerTimer = new Timer(timerId, "created db worker")
    workerTimer.start()
    const worker = await createDbWorker(
        configs,
        workerUrl.toString(),
        wasmUrl.toString(),
        opts.maxBytesToRead,
    );
    workerTimer.end()
    return worker
}

export async function getDb(props: Props, opts: Opts = {}): Promise<Db> {
    const worker = await getWorker(props, opts)
    return worker.db
}

export type UseDb = { db: Db | null, dbId: number, }

export function useDb(props: Props, opts: Opts = {}): UseDb {
    // Naively storing a Db in React state causes an error in obfuscated sqlite.worker.js code I've not gotten to the
    // bottom of. Work around it by storing the Db in a ref, and using a state variable to force a re-render when the Db
    // is initialized.
    const ref = useRef<Db | null>(null);
    const [ dbId, setDbId ] = useState(0);
    useDeepCompareEffect(
        () => {
            async function initDb() {
                const { openTimer } = opts
                if (openTimer) {
                    openTimer.start()
                }
                ref.current = await getDb(props, opts).catch(err => {
                    console.error(`Error opening db:`, err, "props:", props, "opts:", opts)
                    return null
                })
                setDbId(dbId => dbId + 1)
                console.log(`setDbId(${dbId + 1}):`, props, opts)
            }
            initDb()
        },
        [ props, opts ]
    )
    const db = useMemo(() => ref.current, [ dbId ])
    return { db, dbId }
}
